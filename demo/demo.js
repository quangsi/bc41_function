function sayHello() {
  console.log("Chào anh");
  console.log("Chúc 1 ngày tốt lành");
}
// sayHello();
// sayHello();
// sayHello();
// sayHello();
// sayHello();
// parameter;
function sayHelloByName(username, message) {
  console.log("Chào ", username);
  console.log(message);
}

sayHelloByName("Alice", "Hello");
sayHelloByName("Bob", "Hi");
sayHelloByName("Goodbye", "Tomy");

function tinhDTB(toan, van) {
  var dtb = (toan + van) / 2;

  if (dtb == 10) {
    return "Giỏi  quá";
  } else {
    return dtb;
  }
}
tinhDTB("3", 5);
tinhDTB(5, 5);

var dtb1 = tinhDTB(7, 8);

console.log(`  🚀 __ file: demo.js:29 __ dtb1`, dtb1);

var dtb2 = tinhDTB(10, 10);

console.log(`  🚀 __ file: demo.js:38 __ dtb2`, dtb2);

// function literal

var login = function (username, password) {
  console.log("đăng nhập", username, password);
};
login("alice", 123456);
