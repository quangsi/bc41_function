const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";

var btnTinh = document.getElementById("btn-tinh-tien");

function tinhGiaTienKmDauTien(loaiXe) {
  if (loaiXe == UBER_CAR) {
    return 8000;
  }
  if (loaiXe == UBER_SUV) {
    return 9000;
  }
  if (loaiXe == UBER_BLACK) {
    return 10000;
  }
}
function tinhGiaTienKm1Den19(car) {
  if (car == UBER_CAR) {
    return 7500;
  }
  if (car == UBER_SUV) {
    return 8500;
  }
  if (car == UBER_BLACK) {
    return 9500;
  }
}
function tinhGiaTienKm19TroDi(loaiXe) {
  switch (loaiXe) {
    case UBER_CAR: {
      return 7000;
    }
    case UBER_SUV: {
      return 8000;
    }
    case UBER_BLACK: {
      return 9000;
    }
  }
}
//  cần break 1
// ko cần 0
btnTinh.addEventListener("click", function () {
  var carOptionEl = document.querySelector('input[name="selector"]:checked');
  if (carOptionEl == null) {
    // dừng chương trình nếu user không chọn loại xe
    return;
  }
  var car = carOptionEl.value;
  var km = document.getElementById("txt-km").value * 1;
  var giaKmDauTien = tinhGiaTienKmDauTien(car);
  var giaTienKm1Den19 = tinhGiaTienKm1Den19(car);
  var giaTienKm19TroDi = tinhGiaTienKm19TroDi(car);

  var tongTien;
  if (km <= 1) {
    tongTien = giaKmDauTien * km;
  } else if (km <= 19) {
    tongTien = giaKmDauTien * 1 + (km - 1) * giaTienKm1Den19;
  } else {
    tongTien =
      giaKmDauTien + 18 * giaTienKm1Den19 + (km - 19) * giaTienKm19TroDi;
  }
  ("Black 15km");

  console.log(`  🚀 __ file: index.js:17 __ giaKmDauTien`, giaKmDauTien);
});
// lỗi 1
// ko lỗi 0
